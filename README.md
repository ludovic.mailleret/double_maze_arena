# Image analysis pipeline for the double spiral maze introduced in Cointe et al. (2022)

## Presentation of the project's content

This project contains all the different files needed to automatically import a sequence of high resolution images, detect the individuals across consecutive images, reconstruct the spiral topology and assign linear distances to individual detections, and process/filter the data obtained after automatic detection. 

The first part is to be conducted with ImageJ/Fiji. It involves two macros. The Macro_skeleton.ijm macro creates a skeleton along the path of the double spiral maze and splits it to extract a series of particles, whose distance to the centre is then computed iteratively. The macro relies on a java plugin (file Spirou.java) for faster computation. The plugin must be installed as usual ImageJ plugins (see below). 

The second part involves analyzing the sequence of timelapse images and detect on each the particles of interest (moving individuals). It involves background computation and substraction on a moving window, and image segmentation. This part can be started (starting with a file explorer to load the sequence of images) using the macro in file Principal_Macro.ijm.

Once the different tables containing the information on the particles automatically detected after using the ImageJ software have been retrieved, they can be loaded and filtered using the following R script RFilters_MazeData.R Once the data has been filtered, you can use the compressed data table entitled "TableToRunAlltheappliedResults" with the script "ScriptFromFilteringtoFigures" to obtain all the figures and results presented in the paper. The technical results in Figure 5 are to be processed with the following two data tables: "WSDetecTrichosMacro_ACJY144" and "WSDetecTrichos_ACJY144".

## Use of the different ImageJ files for particle detection 

In order to use the various ImageJ macros, it is necessary to call the codes using the following sequence of actions in ImageJ:

`Plugins > Macros > Run` 

Then select the right macro file to be used.

The Spirou plugin can be installed using Plugin>Compile and Install...

## Use of the R script for data filtering 

In the R script, you will be first invited to install several packages and then load the associated libraries.

Then you will combine the different tables obtained with the automatic pipeline and associate for each detected particle, its distance to the centre using the data of the skeleton particles.

Then the script will allow you to apply the different filters. 

Further informations can be found within the source files as comments.


