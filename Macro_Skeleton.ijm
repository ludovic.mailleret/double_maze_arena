// This macro uses an iamge (typically th efirst in a sequence) to define a central path along the double spiral tunnel, which is called the skeleton. Using this skeleton, we will be able to transform (x,y) coordiantes of insect detections into linear (signed) distances from thze center of the spiral, taking into account the topology of the maze.

//-----------------------------------MAKE THE SPIRAL MAZE SKELETON TO LATER COMPUTE THE DISTANCE OF EACH INSECT FROM THE CENTER OF THE MAZE -------------------------

// The image (one picture from the timelapse image sequence) is now duplicated to run the lines corresponding to the skeletonization of the spiral on a dedicated picture
// We name the duplicate image MrSkeleton
imTitle = getInfo("image.title");
IJ.log(imTitle);
run("Duplicate...", "title=MrSkeleton");

run("8-bit");
run("Properties...", "channels=1 slices=1 frames=1 unit=[] pixel_width=1 pixel_height=1 	voxel_depth=1.0000000");
wiwi2 = getWidth();
hehe2 = getHeight();

// this makes the skeleton and exports positions
run("Median...", "radius=5");
// we detect the spiral
setAutoThreshold("Intermodes dark");
run("Analyze Particles...", "size=10000-Infinity show=Nothing add");
// here we can instruction to ignore particles on the edge: only the spiral will thus be detected
resetThreshold();

nba = roiManager("count");

// we fill the particle and clear the rest 
thearenas = newArray(nba);
roiManager("Select", thearenas);
// we shrink the selection to avoid weird contours at the border (optional) 
run("Enlarge...", "enlarge=-20 pixel");
// above line can be adjusted or even suppressed
setForegroundColor(255, 255, 255);
run("Fill");
run("Make Inverse");
setBackgroundColor(0,0,0);
run("Clear");
run("Select None");


// now we skeletonize
run("Make Binary");
run("Select None");
roiManager("reset");
run("Invert LUT");
run("Invert");
run("Skeletonize");



// and then proceed with fragmentation of the skeleton
run("Invert LUT");
run("Grid...", "grid=Lines area=900 color=White");
run("Flatten");
run("8-bit");
run("Make Binary");
run("Invert");

// detect fragments
run("Set Measurements...", "area center redirect=None decimal=3");
run("Analyze Particles...", "display add");

// now run Spirou.java plugin to get the results table
run("Spi rou");

// export result table as file
selectWindow("Results"); 
saveAs("Results", imTitle + "skeleton.tsv");

//The Results table and the roiMAnager are now cleaned so that the data which will be obtained afterwards won't interfere with the previous ones
run("Clear Results");
roiManager("reset");


