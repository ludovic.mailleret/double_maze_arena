// Vincent Calcagno
// Version: 	July 2019
// adds reporting of realized threshold values for particle detection

macro "Super Arenas [è]" {



// ----------------------     SETTINGS     ---------------------------------------------------

// this defines how many images can be open at once (operational stack size)
ssize = 60;
// this defines the scaling factor to be used (100 = no scaling down)
scalar = 100;
// this defines the targeted number of arenas on each image
nbguess = 1;
nbcols = 1;
nblines = 1;
// this defines the thresholding algorithm to use
algo =  "MaxEntropy";
// this defines the radius for the initial blurring (arena delineation)
blurad = 30;
medrad = 2;
// this defines the number of iamges to base the background on
// for the moving window (MUST BE AN ODD NUMBER)
xx = 17;
radius = floor(xx/2);

// -------------------      INITIALISATION     --------------------------------------------------------------

// we first ask the folder in which the image sequence is located
directo = getDirectory("Images to analyze");
// we determine the total number of images to proceed in the folder
filou = getFileList(directo);
nbi = filou.length;
// we determine the number of (full) substacks this will necessitate
nbiter = floor((nbi-1)/(ssize+1-xx));


// we enter batch mode now
setBatchMode(false); 

// we then open the first lot of images
run("Image Sequence...", "open=["+directo+filou[0]+"] number="+ssize+" starting=1 increment=1 scale="+scalar+" file=[] sort");

// Clear dimensions
run("Properties...", "channels=1 slices="+ssize+" frames=1 unit=pixel pixel_width=1 pixel_height=1 voxel_depth=1 frame=[0 sec] origin=0,0");

// Name stack, corresponds to the stack of images which has been loaded previously and it is now renamed
rename("zeStack");   

// get image name and directory
 function getTag(tag) {
      info = getImageInfo();
      index1 = indexOf(info, tag);
      if (index1==-1) return "";
      index1 = indexOf(info, ":", index1);
      if (index1==-1) return "";
      index2 = indexOf(info, "\n", index1);
      value = substring(info, index1+1, index2);
      return value;
  }
  
imPath = getTag("Path");
imTitle = File.nameWithoutExtension;
index = lastIndexOf(imPath, "/");
director = substring(imPath, 0, index);
log(imTitle);

// extract image size and other specs
wiwi = getWidth();
hehe = getHeight();

// ------------------------------------        STEP 1: ARENA LOCATION     ----------------------------------

// From the first image we first locate the arenas
// and save them in the ROI Manager

// an arena is supposed to be at least one tenth of the total surface
area = getWidth()*getHeight()/10;

run("Duplicate...", "title=ZeFirst");



// here we threshold the image in several portions (one per arena), iot improve thresholding in the presence of light heterogeneity
xlength= floor(wiwi/nbcols);
ylength= floor(hehe/nblines);
if(xlength < wiwi/nbcols) xlength = xlength+1;
if(ylength < hehe/nblines) ylength = ylength+1;
print(xlength);
print(ylength);
xinf= newArray(2);
xinf[1] = xlength-1;
xsup= newArray(2);
xsup[0] = xlength-1; xsup[1] = wiwi-1;
yinf= newArray(2);
yinf[1] = ylength-1;
ysup= newArray(2);
ysup[0] = ylength-1; ysup[1] = hehe-1;
//selectWindow("zeStack");
//run("Re Colo");
//run("8-bit");
//run("Select None");

for (l=0; l<nblines; l++) for (c=0; c<nbcols; c++) {
selectWindow("zeStack");
run("Duplicate...", "title=ZeSecond");
run("8-bit");
makeRectangle(xinf[l], yinf[c], xsup[l], ysup[c] );
setAutoThreshold("Default dark");
run("Convert to Mask");
makeRectangle(xinf[l], yinf[c], xsup[l], ysup[c] );
run("Copy");
close("ZeSecond");
selectWindow("ZeFirst");
makeRectangle(xinf[l], yinf[c], xsup[l], ysup[c] );
run("Paste");
}

//-----------------------------------MAKE THE SKELETON TO LATER DEDUCE THE DISTANCE OF EACH TRICHOGRAM FROM THE CENTER-------------------------

// ZeFirst is now duplicated to run the lines corresponding to the skeletonization of the spirale on a dedicated picture
selectWindow("ZeFirst");
run("Duplicate...", "title=MrSkeleton");

run("8-bit");
run("Properties...", "channels=1 slices=1 frames=1 unit=[] pixel_width=1 pixel_height=1 	voxel_depth=1.0000000");
wiwi2 = getWidth();
hehe2 = getHeight();

// this makes the skeleton and exports positions
run("Median...", "radius=5");
// we detect the spiral
setAutoThreshold("Intermodes dark");
run("Analyze Particles...", "size=10000-Infinity show=Nothing add");
// here we can instruction to ignore particles on the edge: only the spiral will thus be detected
resetThreshold();

nba = roiManager("count");

// we fill the particle and clear the rest 
thearenas = newArray(nba);
roiManager("Select", thearenas);
// we shrink the selection to avoid weird contours at the border (optional) 
run("Enlarge...", "enlarge=-20 pixel");
// above line can be adjusted or even suppressed
setForegroundColor(255, 255, 255);
run("Fill");
run("Make Inverse");
setBackgroundColor(0,0,0);
run("Clear");
run("Select None");


// now we skeletonize
run("Make Binary");
run("Select None");
roiManager("reset");
run("Invert LUT");
run("Invert");
run("Skeletonize");



// and then proceed with fragmentation of the skeleton
run("Invert LUT");
run("Grid...", "grid=Lines area=900 color=White");
run("Flatten");
run("8-bit");
run("Make Binary");
run("Invert");

// detect fragments
run("Set Measurements...", "area center redirect=None decimal=3");
run("Analyze Particles...", "display add");

// now run Spirou.java plugin to get the results table
run("Spi rou");

// export result table as file
selectWindow("Results"); 
saveAs("Results", imTitle + "skeleton.tsv");

//The Results table and the roiMAnager are now cleaned so that the data which will be obtained afterwards won't interfere with the previous ones
run("Clear Results");
roiManager("reset");



// --------------------- STEP 1b: COMPUTATION OF THE DISTANCE MAP FOR THE ARENA----------------------------------

// We clear the inside of each arena (to ignore holes in the subsequent distance computation)

selectWindow("ZeFirst");

run("8-bit");
run("Properties...", "channels=1 slices=1 frames=1 unit=[] pixel_width=1 pixel_height=1 	voxel_depth=1.0000000");

wiwi2 = getWidth();
hehe2 = getHeight();

setAutoThreshold("Intermodes dark");
run("Analyze Particles...", "size=10000-Infinity show=Nothing add");
// here we can instruction to ignore particles on the edge: only the spiral will thus be detected
resetThreshold();

nba = roiManager("count");

// we fill the particle and clear the rest 
thearenas = newArray(nba);
roiManager("Select", thearenas);
// we shrink the selection to avoid weird contours at the border (optional) 
run("Enlarge...", "enlarge=-20 pixel");
// above line can be adjusted or even suppressed
setForegroundColor(255, 255, 255);
run("Fill");
run("Make Inverse");
setBackgroundColor(0,0,0);
run("Clear");
run("Select None");

// we record the number of arenas

// areas corresponding to arenas are now recorded in the ROI manager

whichou =  newArray(nba);
for (a=0; a<nba; a++) whichou[a]=a;
roiManager("Select", whichou);

setBackgroundColor(0, 0, 0);
run("Clear Outside");
setBackgroundColor(255, 255, 255);
run("Clear");

run("Select None");
// we add a thin layer outside just in case the arenas are cut at the border
//run("Canvas Size...", "width=" + wiwi+2 +" height=" + hehe+2 + " position=Center zero");
run("Distance Map");
// remove layer
//run("Canvas Size...", "width="+ wiwi+ " height=" + hehe +" position=Center zero");


// ----------------------- STEP 2: BACKGROUND SUBTRACTION AND PARTICLE DETECTION----------------------------

selectWindow("zeStack");
// conversion and inversion
// Instead of working in 8-bit, we use the difference of brightness and saturation
// this is done through a java plugin called Re_Colo
//run("Re Colo");
run("8-bit");
run("Invert", "stack");	

// we'll need to work with a temporary stack for background substractions
run("Duplicate...", "title=tempStack duplicate range=1-");

// we keep track of the overall position (number of images TREATED, in the entire image sequence)
posit = 0;
// current substack number
nn=1;
// used to signal the fact that we are in the last substack
islast = false;
// used to stop the loop
goon=true;



// we now run the treatment of substacks
// we treat substacks one after the other
while (goon) {
	call("java.lang.System.gc"); 
	goon=!islast;
	if (nn==1) initS(); // we init the first substack if relevant
	// in all cases, substract backgrounds for rest of the stack
	proceedS(nn,islast);
	// now analyze particles on relevant slices of current substack
	// and update master index
	posit = particS(nn, posit, islast);
	// now we need to update the substack to move to the next loop, if relevant
	IJ.log("loop");
	if(!islast) islast = updateS(nn,posit); else {close("ZeStack"); close("tempStack");}
	nn=nn+1;
}

IJ.renameResults("Particles.");
//saveAs("Particles.", imTitle + "clean.tsv");



// compute distances to edge
getDistances();
//close("ZeFirst");


// we exit batch mode now
setBatchMode(false); 

// finalize
//redraw();


// -------------------------------------- functions for substacks handling
function proceedS(nn,last) {
	// proceeds the current (sub)stack of images (i.e. applies background substraction)
	// iterate through the stack
	offset = 1;
	if (nn==1) offset=2; // if first substack one more image has already been processed by initS()
	for (i=(radius+offset); i<=(nSlices-radius); i++) {
	selectWindow("zeStack");
	// compute the current background image
	run("Z Project...", "start="+ (i-radius) +" stop="+ (i+radius) +" projection=[Min Intensity]");
	// we name it background
	rename("background");
	// substract
	selectWindow("tempStack");
	setSlice(i);
	imageCalculator("Subtract", "tempStack","background");
	
	setSlice(i-radius); lab1 = getInfo("slice.label");
	setSlice(i+radius); lab2 = getInfo("slice.label");
	setSlice(i); lab3 = getInfo("slice.label");
	print("Background made from "+lab1+" to "+lab2+" for image "+lab3);
	
	// we can now close the background, unless we need it for the last images (for last substack only)
	if (!(last && i==(nSlices-radius))) close("background");
	}
	if(last) {
	// this is the last substack: the final images must also be treated (with the suboptimal background)
	selectWindow("tempStack");
	// iterate through the rest of stack to substract
	for (i=(nSlices-radius+1); i<= nSlices; i++) {
		setSlice(i);
		imageCalculator("Subtract", "tempStack","background");
	}
	// we can now close the background
	close("background");
	}
	// here we reach the border of the current stack
}

function initS() {
	// proceeds the beginning of the initial substack, for which a suboptimal background has to be used
	// note: the first image for which the background is optimal is treated here as well, for efficency
	// compute the suboptimal background
	selectWindow("zeStack");
	run("Z Project...", "start="+ 1 +" stop="+ xx +" projection=[Min Intensity]");
	rename("background");
	selectWindow("tempStack");
	// iterate through the stack to substract
	for (i=1; i<=(radius+1); i++) {
		setSlice(i);
		imageCalculator("Subtract", "tempStack","background");
	}
	// we close the background
	close("background");
}


function updateS(nn,pos) {
	// updates the current substack
	// we get rid of the temporary stack
	close("tempStack");
	// we extract the last slices that should be kept
	selectWindow("zeStack");
	run("Make Substack...", "slices="+(ssize-xx+2)+"-"+ssize);
	rename("Substack");
	// dispose of former stack
	close("zeStack");
	// import the next set of images
	run("Image Sequence...", "open=["+directo+filou[0]+"] number="+(ssize-xx+1)+" starting="+ (pos+radius+1) +" increment=1 scale="+scalar+" file=[] sort");
	rename("sequel");
	print("Loading images "+(pos+radius+1)+" through "+(ssize-xx+1+pos+radius));
	// Clear dimensions
	run("Properties...", "channels=1 slices="+nSlices+" frames=1 unit=pixel pixel_width=1 pixel_height=1 voxel_depth=1 frame=[0 sec] origin=0,0");
	// conversion and inversion
	// Instead of working in 8-bit, we use hte difference between brightness and saturation
	// this is done through a java plugin called Re_Colo
	//run("Re Colo");
	run("Median...", "radius=medrad");
	run("8-bit");
	
	run("Invert", "stack");	
	// combine the two parts
	run("Concatenate...", "  title=[zeStack] image1=Substack image2=sequel image3=[-- None --]");
	// last, we prepare the new temporary stack
	run("Duplicate...", "title=tempStack duplicate range=1-");
	// we are ready to go
	// report on substack status
IJ.log( toString(nn) +" / "+ toString(pos) + " / "+ toString(nSlices) + " / "+ toString(ssize) );	if (nSlices < (ssize) || ((pos+radius+1) + (ssize-xx+1)) >= nbi) {
		// this is the final substack, we must flag this
		return(true);
	} else return(false);
}


function particS(nn, pos, last) {
	// function for particle analysis on the substack
	// we work on tempStack, which for simplicity has the same size as ZeStack, although not all images are useful
	// useless images are first removed
	selectWindow("tempStack");
	// we first delete the slices that should be omitted from analysis
	if (nn != 1) for (i=1; i<=radius; i++) {  // we are not in the first substack: remove the first radius images
		setSlice(1);
		run("Delete Slice");
	}
	if (!last) for (i=1; i<=(radius); i++) {// we are not in the last substack: remove the last radius images		
		setSlice(nSlices);
		run("Delete Slice"); 
	}
	ns = nSlices;
	print(ns+" slices proceeded for substack "+nn+". Curr pos = "+pos);
	// we now apply the automatic threshold algorithm to each remaining image in the stack
	// stack uses the complete histogram (may be more robust)
	setAutoThreshold(algo+" dark");
	//setThreshold(75, 255);
	
lolo=0;
upup=0;
getThreshold(lolo,upup)
;


print(lolo+ " / "+ upup);
															//dd=getNumber("Patience", 0);

	// we remove the parts outside the arenas (by filling these parts in black)
	thearenas = newArray(nba);
	for (a=0; a<nba; a++) thearenas[a]=a;
	roiManager("Select", thearenas);
	if (nba>1) roiManager("OR");
	run("Make Inverse");
	setBackgroundColor(0,0,0);
	run("Clear", "stack");
	run("Select None");
	// get the current number of results

	// We can now launch the particle analyzer
	// analysis will be done per arena (ROI), and a column "arena" is then added to results
	run("Set Measurements...", "area mean centroid shape stack redirect=None decimal=3");
	for (a=0; a<nba; a++) {
		nrb=nResults();
		roiManager("Select", a);
		run("Analyze Particles...", "size=1-Infinity circularity=0.00-1.00 show=Nothing stack display add");
		nra=nResults();
		// we have to set the arena field
		// at this stage we also have to keep track of the overall position in the naming of slices, otherwise we will get lost
		// we thus add to the "Slice" fields the correct number so as to restore the actual global index
		for (r=nrb; r<nra; r++) {
			setResult("Arena", r, a+1);
			tempr=getResult("Slice", r);
			setResult("Slice", r, (tempr+pos));
		}
		updateResults();
	}
	// update master index
	return(pos+ns);
}

function getDistances() {
	// function that computes for each particle its distance to the arena border
	selectWindow("ZeFirst");
	nnn = roiManager("count");
	allthem = newArray(nnn-nba);
	for (a=nba; a<(nnn-1); a++) allthem[a-nba]=a;
	roiManager("Select", allthem);
	run("Set Measurements...", "mean redirect=None decimal=3");
	roiManager("Measure");
	IJ.renameResults("Distances to border")

}

// ----------------------------------------------------------------------------------------




// ------------------------------- Functions to draw the results in a nice way

if (false) {
nbi=152;
nba=4;
wiwi=4916;
hehe = 7916;
reduce=1;
redraw();
}

function redraw() {
	//--------- SETTINGS
	minsize = 8;
	// minimum particle size
	picklargest = true;
	// if true, only the largest particle in each arena is shown
	epaiss = 3;
	// size of drawings
	// we first recreate an empty stack, but of lighter weight
	reduce = 10;
	newImage("reStack", "8-bit white", wiwi, hehe, 1);
	// we first redraw the outline of arenas
	thearenas = newArray(nba);
	for (a=0; a<nba; a++) thearenas[a]=a;
	roiManager("Select", thearenas);
	if (nba>1) nbroiManager("OR");
	run("Make Inverse");
	setBackgroundColor(0,0,0);
	run("Clear", "stack");
	run("Select None");
	// we then scale down
	run("Scale...", "x=1/"+reduce+" y=1/" + reduce+ "width="+ wiwi/reduce+" height="+ hehe/reduce+" interpolation=Bilinear create title=reStack1");
	run("Select All");
	run("Copy");
	// we can dispose of previous ones
	close("reStack");
	close("reStack1");
	// buid empty stack and paste on each slice
	newImage("reStack", "8-bit white", wiwi/reduce, hehe/reduce, nbi);
	for (i=0; i<nbi; i++) {
		setSlice(i+1);
		run("Paste");
	}
	// prepare to draw
	// set colors through LUT
	rr=newArray(256);
	gg=newArray(256);
	bb=newArray(256);
	rr[255]=255; gg[255]=255;bb[255]=255; 
	rr[1]=0; gg[1]=255;bb[1]=255; 
	rr[2]=255; gg[2]=0;bb[2]=0; 
	rr[3]=0; gg[3]=255;bb[3]=0;
	rr[4]=0; gg[4]=0;bb[4]=255;  
	setLut(rr,gg,bb);
	run("Line Width...", "line=5");
	// and now we can draw circles corresponding to the particles
	nr=nResults();
	for (i=0; i<nr; i++) {
	if (getResult("Area",i)>=minsize) {
		xx=getResult("X", i);
		yy=getResult("Y", i);
		ss = getResult("Slice", i);
		// we use the centroid as origin and rescale coordinates appropriately
		aren = getResult("Arena", i);
		setColor(aren);
		setSlice(ss);
		run("Select None");
		drawOval(xx/reduce-(epaiss/2), yy/reduce-(epaiss/2), epaiss, epaiss);
	}
	}

	// make animation
	run("AVI... ", "compression=JPEG frame=15 save=reStack.avi");
}

function drawtraj(from, to) {
	//--------- SETTINGS
	minsize = 8;
	// minimum particle size
	picklargest = true;
	// if true, only the largest particle in each arena is shown
	epaiss = 3;
	// size of drawings
	// we first recreate an empty image, but of lighter weight
	reduce = 10;
	newImage("reStack", "8-bit white", wiwi, hehe, 1);
	// we first redraw the outline of arenas
	thearenas = newArray(nba);
	for (a=0; a<nba; a++) thearenas[a]=a;
	roiManager("Select", thearenas);
	if (nba>1) roiManager("OR");
	run("Make Inverse");
	setBackgroundColor(0,0,0);
	run("Clear", "stack");
	run("Select None");
	// we then scale down
	run("Scale...", "x=1/"+reduce+" y=1/" + reduce+ "width="+ wiwi/reduce+" height="+ hehe/reduce+" interpolation=Bilinear create title=reStack1");
	run("Select All");
	run("Copy");
	// we can dispose of previous ones
	close("reStack");
	close("reStack1");
	// buid empty image
	newImage("reStack", "8-bit white", wiwi/reduce, hehe/reduce, 1);
	run("Paste");
	
	// we want to sort the results table by slice
	// first get the ranks for the slice column
	
	
	// prepare to draw
	// set colors through LUT
	rr=newArray(256);
	gg=newArray(256);
	bb=newArray(256);
	rr[255]=255; gg[255]=255;bb[255]=255; 
	rr[1]=0; gg[1]=255;bb[1]=255; 
	rr[2]=255; gg[2]=0;bb[2]=0; 
	rr[3]=0; gg[3]=255;bb[3]=0;
	rr[4]=0; gg[4]=0;bb[4]=255;  
	setLut(rr,gg,bb);
	run("Line Width...", "line=5");
	
	// and now we can draw circles corresponding to the biggest particle in each arena
	// and lines in between successive slices
	nr=nResults();
	currResults= newArray(nba);
	// this will store the current index of the largest particle, per slice and per arena
	currAreas= newArray(nba);
	pastR = newArray(nba);
	for (k=0; k<nba; k++) {
		currResults[k]=0;
		currAreas[k]=0;
		pastR[k]=0;
	}
	// this will record the previous result, to draw the line segment
	currSlice =1;
	for (i=0; i<nr; i++) {
		aa = getResult("Area",i);
		if (aa >=minsize) {
			ss = getResult("Slice", i);
			if (ss == currSlice) {
				aren = getResult("Arena", i);
				if (aa>currAreas[aren-1]) {
					// this one is larger, use it instead
					currResults[aren-1] = i;
					currAreas[aren-1] = aa;
				}
			} else if (ss>currSlice) {
				// we have moved on to a new slice: we use the current best results
				xx = newArray(nba);
				yy = newArray(nba);
				for (k=0; k<nba; k++) {
					ii = currResults[k];
					xx=getResult("X", ii);
					yy=getResult("Y", ii);
					setColor(k+1);
					// draw point at new particle
					drawOval(xx/reduce-(epaiss/2), yy/reduce-(epaiss/2), epaiss, epaiss);
					// draw line segment
					drawLine(getResult("X",pastR[k]), getResult("Y",pastR[k]), xx,yy);
					// update pastR and clear currResults/currAreas
					pastR[k] = ii;
					currResults[k]=0;
					currAreas[k]=0;
				}
				// update currSlice
				currSlice = ss;
			}
		}
		}


}





// ------------------------ Function to locate eggs and patches

function getEggs2() {
	// uses color information to spot patches (much easier)
	// ------ SETTINGS
	nbeggs = 36;
	// target number of eggs per arena
	inittol = 30;
	// initial noise tolerance, will be decreased until we get enough peaks
	// we shrink the selection to exclude borders
	run("Scale... ", "x=0.95 y=0.95 centered");
	

}


function getEggs() {
	// ------ SETTINGS
	nbeggs = 36;
	// target number of eggs per arena
	inittol = 30;
	// initial noise tolerance, will be decreased until we get enough peaks
	// we shrink the selection to exclude borders
	run("Scale... ", "x=0.95 y=0.95 centered");
	rename("zeStack");
	ns=nSlices();
	// first pass : we select noise tolerance so that we get just above the target number of eggs
		tol=inittol;
		nbfound=0;
		up=true;
		first=true;
		stop=false;
		selectWindow("zeStack");
		while(!stop) {
		run("Clear Results");
		run("Find Maxima...", "noise="+tol+" output=List exclude light");
		nbfound = nResults();
		neodiff=nbfound-nbeggs;
		if (neodiff>0) {
			// we have too many
			if(!up && !first) {
				// we had too few before: settle for the too many
				stop=true;
			} else {
			tol=tol+1;
			up=true;
			}
			} else if (neodiff<0) {
			// too few
			tol=tol-1;
			if(up && !first) {
				// we had too many before: we settle for too many
				run("Find Maxima...", "noise="+tol+" output=List exclude light");
				nbfound = nResults();
				stop=true;
			} else up=false;
			} else {
			// perfect!
			stop=true;
			}
			first=false;
		}
		print("Using "+tol);
		// now the whole trick is to identify patches and exclude the junk in between!
		// for this we look for clumps and a pattern
		
		
}
	// now we worn on heat which is a consensus




function getEggs() {
	// ------ SETTINGS
	nbeggs = 36;
	// target number of eggs per arena
	inittol = 30;
	// initial noise tolerance, will be decreased until we get enough peaks
	//run("Enlarge...", "enlarge=-100");
	rename("zeStack");
	ns=nSlices();
	newImage("heat", "8-bit white", getWidth(), getHeight(), 1);
	// first pass for each slice
	for (i=0; i<ns; i++) {
		tol=inittol;
		nbfound=0;
		selectWindow("zeStack");
		setSlice(i+1);
		while(nbfound<nbeggs+10 && tol>2) {
		run("Clear Results");
		run("Find Maxima...", "noise="+tol+" output=List exclude light");
		nbfound = nResults();
		tol=tol-1;
		}
		print("Using "+tol);
		// add info to heatmap
		nba = nResults();
		selectWindow("heat");
		for (j=0; j<nba; j++) {
			xx=getResult("X", j);
			yy=getResult("Y", j);
			setPixel(xx,yy,getPixel(xx,yy)-10);
		}
	}
	// now we worn on heat which is a consensus
}


// -------------------------------------------------- END OF FILE
}
